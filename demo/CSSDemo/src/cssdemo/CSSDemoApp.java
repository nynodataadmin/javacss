/*
 * CSSDemoApp.java
 */

package cssdemo;

import java.net.*;
import com.sun.stylesheet.*;
import com.sun.stylesheet.css.*;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

/**
 * The main class of the application.
 */
public class CSSDemoApp extends SingleFrameApplication {
    private static Stylesheet global;
    
    /**
     * At startup create and show the main frame of the application.
     */
    @Override protected void startup() {
        show(new CSSDemoView(this));
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override protected void configureWindow(java.awt.Window root) {
        global.applyTo(root);
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of CSSDemoApp
     */
    public static CSSDemoApp getApplication() {
        return Application.getInstance(CSSDemoApp.class);
    }

    /**
     * Main method launching the application.
     */
    public static void main(String[] args) throws Exception {
        Stylesheet.enableDebugging();
        URL stylesheet = CSSDemoApp.class.getResource("resources/main.css");
        global = CSSParser.parse(stylesheet);
        launch(CSSDemoApp.class, args);
    }
}
