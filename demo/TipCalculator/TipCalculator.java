import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import java.text.*;

import com.sun.stylesheet.*;
import com.sun.stylesheet.css.*;

public class TipCalculator extends JFrame {
    private NumberFormat formatter;
    private JFormattedTextField amount;
    private JSlider slider;
    private JLabel totalValue;
    private JLabel title;
    private Stylesheet currentStylesheet;

    public TipCalculator() {
        super("Tip Calculator");
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 1;
        c.anchor = GridBagConstraints.WEST;
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.insets = new Insets(3, 3, 3, 3);
        title = new JLabel("Tip Calculator");
        title.setName("title");
        title.putClientProperty("styleClass", "bold");
        add(title, c);
        
        c.gridx = GridBagConstraints.RELATIVE;
        c.gridwidth = 1;
        c.anchor = GridBagConstraints.EAST;
        JLabel amountLabel = new JLabel("Amount:");
        amountLabel.setName("amountLabel");
        add(amountLabel, c);
        
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.fill = GridBagConstraints.HORIZONTAL;
        formatter = NumberFormat.getCurrencyInstance();
        amount = new JFormattedTextField(new NumberFormatter(formatter));
        amount.setName("amount");
        amount.setValue(20);
        amount.addPropertyChangeListener("value", new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                calculateTip();
            }
        });
        add(amount, c);
        
        c.gridwidth = 1;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.NORTHEAST;
        c.insets = new Insets(10, 3, 3, 3);
        JLabel tipLabel = new JLabel("Tip:");
        tipLabel.setName("tipLabel");
        add(tipLabel, c);
        
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.insets = new Insets(3, 3, 3, 3);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        slider = new JSlider(0, 30, 15);
        slider.setName("tip");
        slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                calculateTip();
            }
        });
        add(slider, c);

        c.gridwidth = 1;
        c.anchor = GridBagConstraints.EAST;
        c.fill = GridBagConstraints.NONE;
        c.weightx = 0;
        JLabel total = new JLabel("Total:");
        total.setName("totalLabel");
        total.putClientProperty("styleClass", "bold");
        add(total, c);

        c.anchor = GridBagConstraints.WEST;
        c.gridwidth = GridBagConstraints.REMAINDER;
        totalValue = new JLabel();
        totalValue.setName("total");
        totalValue.putClientProperty("styleClass", "bold");
        add(totalValue, c);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        calculateTip();
        
        try {
            Stylesheet.enableDebugging();
            currentStylesheet = CSSParser.parse(TipCalculator.class.getResource("stylesheet.css"));
            currentStylesheet.setName("Global");
            currentStylesheet.applyTo(this);
        }
        catch (Exception e) { e.printStackTrace();  }
        pack();
    }


    private void calculateTip() {
        Number amountValue = ((Number) amount.getValue());
        if (amountValue != null)
            totalValue.setText(formatter.format(slider.getValue() / 100.0 * 
                    amountValue.doubleValue()));
        else
            totalValue.setText("");
    }


    public static void main(String[] arg) {
        new TipCalculator().setVisible(true);
    }       
}
