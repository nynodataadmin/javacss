Overview
--------

This is the JavaCSS project, which enables CSS styling of Java objects.  For 
example, the stylesheet

* {
  font-family: Helvetica;
  font-size: 14pt;
}

JButton.link {
  border: null;
  text-decoration: underline;
  foreground: blue;
}

JButton.link:armed {
  foreground: red;
}

will set the default font for all components to 14pt Helvetica.  Any JButtons 
with the "link" style class, which is set by calling

button.putClientProperty(Stylesheet.STYLE_CLASS, "link")

will be made to look like HTML links, by removing their borders and setting them 
to underlined blue text.  When the mouse is pressed on one of these buttons 
(making it "armed"), the text will become red, just as you
might see when clicking a link in a web browser.


Build Instructions
------------------

Download from http://ant.apache.org (tested with Ant 1.7).

Run "ant" (to build the jar files) or "ant dist" (to build the distribution zip 
files) from the root of the source distribution.

Built files will be placed into the "build" subdirectory.


Documentation
-------------

Up-to-date documentation can be found at http://javacss.dev.java.net


Licensing
---------

JavaCSS is licensed under the terms of the GNU General Public License version 2.
Please see the LICENSE file for details.
