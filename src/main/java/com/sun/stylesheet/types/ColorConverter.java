/*
 * Copyright 2008 Sun Microsystems, Inc.  All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * CA 95054 USA or visit www.sun.com if you need additional information or
 * have any questions.
 */

package com.sun.stylesheet.types;

import java.awt.*;
import java.lang.reflect.*;
import java.util.regex.*;
 
/**
 * Converts strings into <code>Colors</code>.  Four different forms are 
 * supported:
 * <ol>
 * <li>The case-insensitive name of a constant field in 
 * <code>java.awt.Color</code>, such as <code>red</code> or 
 * <code>lightGray</code>.  <code>Color.red</code> and 
 * <code>java.awt.Color.lightGray</code> are also supported.
 * <li>A hash (#) followed by six hexadecimal digits.  The first two digits are 
 * red, the middle two are green, and the last two are blue.  
 * <code>#ff00ff</code> is the color magenta.
 * <li>A call to <code>new Color(int, int, int)</code> or <code>new Color(int, 
 * int, int, int)</code>.  The string <code>new java.awt.Color(255, 255, 
 * 255)</code> is the color white, and the string <code>new Color(0, 0, 80, 
 * 128)</code> is translucent dark blue.
 * <li>The string <code>null</code>.
 * </ul>
 *
 *@author Ethan Nicholas
 */
public class ColorConverter implements TypeConverter<Color> {
    private Pattern pattern = Pattern.compile("(?:new\\s+(?:java.awt.)?" +
            "Color\\s*\\(\\s*)(\\d+)\\s*,\\s*(\\d+)\\s*,\\s*(\\d+)\\s*" +
            "(?:,\\s*(\\d+)\\s*)?\\)?");


    public Color convertFromString(String string) {
        if (string.equals("null"))
            return null;
        Matcher m = pattern.matcher(string);
        if (m.matches()) {
            String alpha = m.group(4);
            if (alpha != null && alpha.length() > 0)
                return new Color(Integer.parseInt(m.group(1)),
                                 Integer.parseInt(m.group(2)),
                                 Integer.parseInt(m.group(3)),
                                 Integer.parseInt(m.group(4)));
            else
                return new Color(Integer.parseInt(m.group(1)),
                                 Integer.parseInt(m.group(2)),
                                 Integer.parseInt(m.group(3)));
        }
        else if (string.length() == 7 && string.charAt(0) == '#') {
            return new Color(Integer.parseInt(string.substring(1), 16));
        }
        else {
            try {
                if (string.startsWith("java.awt.Color."))
                    string = string.substring("java.awt.Color.".length());
                else if (string.startsWith("Color."))
                    string = string.substring("Color.".length());
                Field color = Color.class.getField(string);
                return (Color) color.get(null);
            }
            catch (NoSuchFieldException e) {
                throw new IllegalArgumentException("colors must be of the " + 
                    "form #xxxxxx ('#' followed by six hexadecimal digits), or the name of a constant field in java.awt.Color (found: '" + string + "')");
            }
            catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }
}