/*
 * Copyright 2008 Sun Microsystems, Inc.  All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * CA 95054 USA or visit www.sun.com if you need additional information or
 * have any questions.
 */

package com.sun.stylesheet.styleable;

import java.awt.*;
import java.util.List;

import com.sun.scenario.scenegraph.*;

import com.sun.stylesheet.*;
import com.sun.stylesheet.types.*;

/**
 * Provides <code>Styleable</code> support for <code>SGNodes</code>.
 *
 *@author Ethan Nicholas
 */
public class SGNodeStyleSupport extends BeanStyleSupport {
    public SGNodeStyleSupport(Class cls) {
        super(cls);
    }


    public String getID(Object object) {
        return ((SGNode) object).getID();
    }
    
    
    public String getStyleClass(Object object) {
        return (String) ((SGNode) object).getAttribute(
                Stylesheet.STYLE_CLASS_KEY);
    }
    

    public Styleable getStyleableParent(Object object) {
        SGNode parent = ((SGNode) object).getParent();
        return parent != null ? TypeManager.getStyleable(object) : null;
    }
    
    
    public Styleable[] getStyleableChildren(Object object) {
        if (object instanceof SGComponent)
            return new Styleable[] { TypeManager.getStyleable(
                    ((SGComponent) object).getComponent()) };
        else if (object instanceof SGParent) {
            List<SGNode> children = ((SGParent) object).getChildren();
            Styleable[] result = new Styleable[children.size()];
            for (int i = 0; i < result.length; i++)
                result[i] = TypeManager.getStyleable(children.get(i));
            return result;
        }
        else
            return new Styleable[0];
    }
}