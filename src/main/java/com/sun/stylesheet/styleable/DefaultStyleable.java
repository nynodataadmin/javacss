/*
 * Copyright 2008 Sun Microsystems, Inc.  All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * CA 95054 USA or visit www.sun.com if you need additional information or
 * have any questions.
 */

package com.sun.stylesheet.styleable;

import com.sun.stylesheet.*;
import com.sun.stylesheet.types.Size;
import com.sun.stylesheet.types.TypeManager;

import java.awt.*;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Default <code>Styleable</code> wrapper which supports a variety of classes.  
 * <code>DefaultStyleable</code> can wrap any class which is supported by 
 * an implementation of {@link StyleSupport}.
 */
public class DefaultStyleable implements Styleable, ContainerListener {
    private static class StylesheetApplication {
        private Stylesheet stylesheet;
        private int depth;
        
        private StylesheetApplication(Stylesheet stylesheet, int depth) {
            this.stylesheet = stylesheet;
            this.depth = depth;
        }
        
        
        public boolean equals(Object o) {
            if (!(o instanceof StylesheetApplication))
                return false;
            
            StylesheetApplication s = (StylesheetApplication) o;
            return s.stylesheet == stylesheet && s.depth == depth;
        }
        
        
        public int hashCode() {
            return stylesheet.hashCode() ^ depth;
        }
    }
    
    static Map<Class, StyleSupport> support = 
            new HashMap<Class, StyleSupport>();

    Object object;
    List<StylesheetApplication> stylesheets = 
            new ArrayList<StylesheetApplication>();
    
    

    public DefaultStyleable(Object object) {
        this.object = object;
        if (object instanceof Container)
            ((Container) object).addContainerListener(this);
    }
    
    
    /**
     * Informs the object that a new stylesheet is being applied to it.  The 
     * <code>depth</code> parameter indicates the "ancestry level" of the style 
     * root (the object to which the stylesheet is being directly applied);  for 
     * instance if the style root is this object's parent, the depth is 1.  The 
     * depth is needed to correctly resolve priorities in the face of 
     * conflicting rules, with lower depths having precedence over higher 
     * depths.
     * <p>
     * Knowing which stylesheets are affecting it allows 
     * <code>DefaultStyleable</code> to automatically apply stylesheets to
     * newly-added children, for objects which provide the necessary 
     * notifications.
     *
     *@param s the stylesheet being applied
     *@param depth the depth of this object below the style root
     *@see #removeStylesheet
     */
    public void addStylesheet(Stylesheet s, int depth) {
        stylesheets.add(new StylesheetApplication(s, depth));
    }
    
    
    /**
     * Informs the object that a stylesheet no longer applies to it.  The 
     * <code>depth</code> parameter indicates the "ancestry level" of the style
     * root (the object to which the stylesheet is being directly applied);  for
     * instance if the style root is this object's parent, the depth is 1.  The 
     * depth is needed to correctly resolve priorities in the face of 
     * conflicting rules, with lower depths having precedence over higher 
     * depths.
     * <p>
     * Knowing which stylesheets are affecting it allows 
     * <code>DefaultStyleable</code> to automatically apply stylesheets to
     * newly-added children, for objects which provide the necessary 
     * notifications.
     *
     *@param s the stylesheet being removed
     *@param depth the depth of this object below the style root
     *@see #addStylesheet
     */
    public void removeStylesheet(Stylesheet s, int depth) {
        stylesheets.remove(new StylesheetApplication(s, depth));
    }
    
    
    private synchronized StyleSupport getStyleSupport() {
        return TypeManager.getStyleSupport(object);
    }
    
    
    public String getID() {
        return getStyleSupport().getID(object);
    }


    public Object getBaseObject() {
        return object;
    }
    
    
    public Class[] getObjectClasses() {
        return getStyleSupport().getObjectClasses(object);
    }
    
    
    public String getStyleClass() {
        return getStyleSupport().getStyleClass(object);
    }
    
    
    public Object convertPropertyFromString(String propertyName, String value) 
            throws StylesheetException {
        Class type = getStyleSupport().getPropertyType(object, propertyName);
        return TypeManager.convertFromString(value, type);
    }


    public Object getProperty(String key) throws StylesheetException {
        return getStyleSupport().getProperty(object, key);
    }


    public void setProperty(String key, Object value) throws StylesheetException {
        getStyleSupport().setProperty(object, key, value);
    }
    
    
    public Styleable getStyleableParent() {
        return getStyleSupport().getStyleableParent(object);
    }


    public Styleable[] getStyleableChildren() {
        return getStyleSupport().getStyleableChildren(object);
    }
    
    
    public boolean isPropertyInherited(String propertyName) 
            throws StylesheetException {
        return getStyleSupport().isPropertyInherited(object, propertyName);
    }
    
    
    public void addPseudoclassListener(String pseudoclass, 
            PseudoclassListener listener) throws StylesheetException {
        getStyleSupport().addPseudoclassListener(this, pseudoclass, listener);
    }
    
    
    public void removePseudoclassListener(String pseudoclass,
            PseudoclassListener listener) throws StylesheetException {
        getStyleSupport().removePseudoclassListener(this, pseudoclass, 
                listener);
    }
    
    
    public void componentAdded(ContainerEvent e) {
        try {
            Styleable object = TypeManager.getStyleable(e.getChild());
            for (StylesheetApplication s : stylesheets)
                s.stylesheet.applyTo(object, s.depth + 1);
            PropertyManager.cascadeTo(object, true);
        }
        catch (StylesheetException ex) {
            throw new RuntimeException(ex);
        }
    }


    public void componentRemoved(ContainerEvent e) {
        try {
            Component c = e.getChild();
            if (c.getClass().getName().endsWith("Hyperlink"))
            {
                int i =0;
            }
            Styleable object = TypeManager.getStyleable(e.getChild());
            PropertyManager.removeAllStyles(object);
        }
        catch (StylesheetException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    
    public Map<String, Object> splitCompoundProperty(String property, 
            Object value) throws StylesheetException {
        Class type = getStyleSupport().getPropertyType(object, property);
        if (type == Font.class) { // todo: make this pluggable
            Font font = (Font) value;
            Map<String, Object> result = new HashMap<String, Object>();
            result.put(property + "-family", font.getFamily());
            result.put(property + "-size", new Size(font.getSize2D(), 
                    Size.Unit.pt));
            result.put(property + "-weight", 
                    (font.getStyle() & Font.BOLD) != 0 ? 
                    FontWeightHandler.FontWeight.bold : 
                    FontWeightHandler.FontWeight.normal);
            result.put(property + "-style", 
                    (font.getStyle() & Font.ITALIC) != 0 ? 
                    FontStyleHandler.FontStyle.italic : 
                    FontStyleHandler.FontStyle.plain);
            return result;
        }
        return null;
    }


    public boolean equals(Object o) {
        if (!(o instanceof DefaultStyleable))
            return false;
        return object == ((DefaultStyleable) o).object;
    }
    
    
    public int hashCode() {
        return object.hashCode();
    }
    
        
    public String toString() {
        return "DefaultStyleable[" + object.getClass().getName() + ", " + 
                System.identityHashCode(object) + ", " + 
                System.identityHashCode(this) + "]";
    }
}