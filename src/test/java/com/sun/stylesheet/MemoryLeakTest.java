package com.sun.stylesheet;

import com.sun.stylesheet.css.CSSParser;
import junit.framework.Assert;
import no.nynodata.netbeans.testcase.NbTestCase;
import org.junit.Test;

import javax.swing.*;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;

public class MemoryLeakTest extends NbTestCase
{
    @Test
    public synchronized void testStylingMemoryLeak() throws InterruptedException, InvocationTargetException
    {
        Reference[] refs = styleLabels();
        assertGC("non-css label", refs[0]);
        assertGC("css label", refs[1]);
    }


    protected Reference[] styleLabels() throws InvocationTargetException, InterruptedException
    {
        final WeakReference[] result = new WeakReference[2];
        Runnable r = new Runnable()
        {
            public void run()
            {
                JLabel labelStyled = new JLabel("styled");
                JLabel labelUnstyled = new JLabel("unstyled");
                URL css = getClass().getClassLoader().getResource("style.css");
                Stylesheet stylesheet = null;
                try
                {
                    stylesheet = CSSParser.parse(css);
                    stylesheet.applyTo(labelStyled);
                    stylesheet.removeFromAll();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }

                WeakReference styledReference = new WeakReference<JLabel>(labelStyled);
                WeakReference unstyledReference = new WeakReference<JLabel>(labelUnstyled);

                labelStyled = null;
                labelUnstyled = null;
                result[0] = unstyledReference;
                result[1] = styledReference;
            }
        };
        SwingUtilities.invokeAndWait(r);
        return result;

    }

    /**
     * Causes the garbage collector to run a number of times.
     */
    public static void stressGC(Reference exitWhenNull)
    {
        ArrayList alloc = new ArrayList();
        int size = 100;
        int outOfMemoryCount = 0;
        int succesfullAllocs = 0;
        while (outOfMemoryCount < 5)
        {
            if (exitWhenNull.get() == null)
                return;

            System.gc();
            System.runFinalization();
            try
            {
                Thread.sleep(21);
            }
            catch (InterruptedException e)
            {
                return;
            }

            System.gc();
            System.runFinalization();
            try
            {
                size = Math.max(size, 100);
                alloc.add(new byte[size]);
                size = (int) (((double) size) * 1.3);
                succesfullAllocs++;
            }
            catch (OutOfMemoryError error)
            {
                size = size / 2;
                if (succesfullAllocs > 0)
                {
                    outOfMemoryCount++;
                    succesfullAllocs = 0;
                    alloc.clear();
                }
            }
        }
        Object ref = exitWhenNull.get();
        if (ref != null)
        {

            Assert.fail("reference not cleared: " + ref + " " + exitWhenNull.isEnqueued() );
        }
    }


}
